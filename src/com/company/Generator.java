package com.company;

import com.company.model.Bank;
import com.company.model.Exchange;

public class Generator {

    public static Bank[] generateBanks(){
        String[] nameBank = {"Bank First", "Bank Second", "Bank 3"};
        int[] courcesBank = {28, 27, 26, 25};
        int[] limit = {100000, 110000, 50000};
        Bank[] banks = new Bank[nameBank.length];
        for (int i = 0; i < banks.length; i++) {
            banks[i] = new Bank(nameBank[i], courcesBank[i], limit[i]);
        }
        return banks;
    }

    public static Exchange[] generateExchanges(){
        String[] nameExchange = {"Exchange1", "Exchange2", "Exchange3"};
        int[] courcesExchange = {28, 27, 26, 25};
        int[] limitExchanges = {30000, 40000, 100000};
        Exchange[] exchanges = new Exchange[nameExchange.length];
        for (int i = 0; i < exchanges.length; i++) {
            exchanges[i] = new Exchange(nameExchange[i], courcesExchange[i], limitExchanges[i]);
        }
        return exchanges;
    }

}
