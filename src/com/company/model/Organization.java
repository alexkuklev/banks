package com.company.model;

public class Organization  {

    public String name;
    public int cource;
    public int limit;

    public Organization(String name, int cource, int limit) {
        this.name = name;
        this.cource = cource;
        this.limit = limit;
    }

    public String getName(){
        return name;
    }

    public String getValue(int valueMoney){
        return  String.format( "%.2d", valueMoney/cource);
    }
}
