package com.company.model;

public class Exchange extends Organization{

    public Exchange(String nameExchange, int courceExchange, int limit) {
        super(nameExchange, courceExchange, limit);
    }

    public String getValue(int amountMoney){
        return String.format("%.2f", (amountMoney/cource) * 0.95);
    }

    public String getName() {
        return name;
    }
}
