package com.company.model;

public class Bank extends Organization{

    public Bank(String nameBank, int cource, int limit) {
        super(nameBank, cource, limit);
    }

    public String getNameBank() {
        return name;
    }


    public String getValue(int amountMoney){
        //return super.getValue(amountMoney) * 0.95;
        return String.format( "%.2f",(amountMoney/cource) * 0.95);
    }
}
