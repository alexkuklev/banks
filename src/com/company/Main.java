package com.company;

import com.company.model.Bank;
import com.company.model.BlackMarket;
import com.company.model.Exchange;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        int ANLIMITED = Integer.MAX_VALUE;
        Bank[] banks = Generator.generateBanks();
        Exchange[] exchanges = Generator.generateExchanges();


        BlackMarket blackMarket = new BlackMarket("MyBlackMarket", 40, ANLIMITED);

        for (Bank bank : banks) {
            System.out.println(bank.name);
        }
        for (Exchange exchange : exchanges) {
            System.out.println(exchange.name);
        }

        System.out.println("Insert value for banks(UAH):");

        Scanner in = new Scanner(System.in);
        int valueFromUser = in.nextInt();
        for (Bank bank : banks) {
            System.out.println(bank.getNameBank() + " value: " + bank.getValue(valueFromUser));
        }

        System.out.println("Insert value for Exchanges:");
        int valueForExchanges = in.nextInt();
        for (Exchange exchange : exchanges) {
            System.out.println(exchange.getName() + " value: " + exchange.getValue(valueForExchanges));
        }

        System.out.println("Insert value for black market:");
        int valueForBlackMarket = in.nextInt();
        System.out.println(blackMarket.getName() + "value: " + String.valueOf(blackMarket.getValue(valueForBlackMarket)));
    }
}
